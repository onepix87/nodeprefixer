const path = require('path');
const join = path.join;
const dirname = path.dirname;
const basename = path.basename;
const extname = path.extname;

module.exports = {
    plugins: [
        [
            'astroturf/plugin',
            {
                tagName: 'css',
                extension: '.css',
                enableCssProp: true,
                writeFiles: true, // Writes css files to disk using the result of `getFileName`
                getFileName(hostFilePath, pluginsOptions) {
                    const basepath = join(
                        dirname(hostFilePath),
                        basename(hostFilePath, extname(hostFilePath)),
                    );
                    return `${basepath}__extracted_style${this.extension}`;
                },
            },
        ],
    ],
};