css`
    body {
        display: -webkit-box;
        display: -ms-flexbox;
        display: flex;
    }
    div {
        display: -ms-grid;
        display: grid;
        -ms-grid-rows: (1fr)[5];
        grid-template-rows: repeat(5, 1fr);
    }
`