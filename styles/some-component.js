import { LitElement, html } from 'lit-element';

export class UcPlpList extends LitElement {
    static get styles() {
        return [css`
:root {
    --some-color: red;
}
`, css`
@media (min-width: 1024px) {
    :host {
        display: -webkit-box;
        display: -ms-flexbox;
        display: flex;
    }
}`, css`
@media (min-width: 768px) {
    :host([type='cards']) {
        display: -ms-grid;
        display: grid;
        color: red;
        -ms-grid-columns: 1fr 1fr 1fr;
        grid-template-columns: 1fr 1fr 1fr;
        grid-auto-rows: minmax(-webkit-min-content, -webkit-max-content);
        grid-auto-rows: minmax(min-content, max-content);
    }
}
`];
    }

    render() {
        return html`
      <slot></slot>
    `;
    }
}
