import { css } from 'lit-element';

export const common = css`
  :host {
    display: block;
    background-color: white;
    position: relative;
    font-size: 1.4rem;
  }
  :host(:hover) {
    -webkit-box-shadow: 0 4px 20px 0 rgba(72, 78, 92, 0.1);
            box-shadow: 0 4px 20px 0 rgba(72, 78, 92, 0.1);
    z-index: 1;
    cursor: pointer;
  }
  .plp-item {
    width: 100%;
    height: 100%;
    display: -webkit-box;
    display: -ms-flexbox;
    display: flex;
    padding: 20px;
    -webkit-box-sizing: border-box;
            box-sizing: border-box;
    cursor: pointer;
    text-decoration: none;
    color: black;
  }
  .plp-item:hover,
  .plp-item:focus {
    text-decoration: none;
    color: black;
  }
  .plp-item__info {
    display: -ms-grid;
    display: grid;
    grid-auto-rows: minmax(-webkit-min-content, -webkit-max-content);
    grid-auto-rows: minmax(min-content, max-content);
    min-height: 0;
    height: 90%;
    grow: 1;
  }
  ::slotted(.plp-item__info__rating) {
    color: #666666;
  }
  ::slotted(.plp-item__info__title) {
    font-size: 1.15em;
    -webkit-transition: color 0.1s ease-in;
    -o-transition: color 0.1s ease-in;
    transition: color 0.1s ease-in;
  }
  ::slotted(.plp-item__info__title:hover) {
    color: #e6615e;
  }
  :host(:not([type='card'])) .plp-item__info,
  :host([type='card']) .plp-item__info {
    grid-row-gap: 8px;
  }
  .add-to-cart {
    position: absolute;
    bottom: 0px;
    right: 0px;
    margin: 20px;
  }
  ::slotted(.plp-item__info__rating),
  ::slotted(uc-product-rating) {
    display: -webkit-box;
    display: -ms-flexbox;
    display: flex;
  }


`;

export const huge = css`
  @media (min-width: 1200px) {
    :host([type='list']) {
      height: 250px;
    }
  }


`;

export const desktop = css`
  @media (min-width: 768px) and (max-width: 1199px) {
    :host([type='list']) {
      height: 240px;
    }
  }
  @media (min-width: 768px) {
    :host([type='card']) {
      max-width: 280px;
      height: 100%;
      min-height: 400px;
      margin-bottom: 14px;
    }
    :host([type='list']) {
      width: 100%;
      height: 240px;
      min-height: 160px;
    }
    :host([type='card']) .plp-item {
      -webkit-box-orient: vertical;
      -webkit-box-direction: normal;
          -ms-flex-direction: column;
              flex-direction: column;
      -webkit-box-pack: start;
          -ms-flex-pack: start;
              justify-content: flex-start;
    }

    :host([type='list']) .plp-item {
      -webkit-box-orient: horizontal;
      -webkit-box-direction: normal;
          -ms-flex-direction: row;
              flex-direction: row;
    }
    :host([type='list']) .plp-item__info {
      -ms-grid-columns: 2fr 20px 1fr;
      grid-template-columns: 2fr 1fr;
      -ms-grid-rows: 1fr 5px 1fr 5px 1fr 5px 1fr 5px 1fr;
      grid-template-rows: repeat(5, 1fr);
      grid-column-gap: 20px;
      grid-row-gap: 5px;
      width: 100%;
    }
    :host([type='list']) .plp-item__info > *:nth-child(1) {
        -ms-grid-row: 1;
        -ms-grid-column: 1;
    }
    :host([type='list']) .plp-item__info > *:nth-child(2) {
        -ms-grid-row: 1;
        -ms-grid-column: 3;
    }
    :host([type='list']) .plp-item__info > *:nth-child(3) {
        -ms-grid-row: 3;
        -ms-grid-column: 1;
    }
    :host([type='list']) .plp-item__info > *:nth-child(4) {
        -ms-grid-row: 3;
        -ms-grid-column: 3;
    }
    :host([type='list']) .plp-item__info > *:nth-child(5) {
        -ms-grid-row: 5;
        -ms-grid-column: 1;
    }
    :host([type='list']) .plp-item__info > *:nth-child(6) {
        -ms-grid-row: 5;
        -ms-grid-column: 3;
    }
    :host([type='list']) .plp-item__info > *:nth-child(7) {
        -ms-grid-row: 7;
        -ms-grid-column: 1;
    }
    :host([type='list']) .plp-item__info > *:nth-child(8) {
        -ms-grid-row: 7;
        -ms-grid-column: 3;
    }
    :host([type='list']) .plp-item__info > *:nth-child(9) {
        -ms-grid-row: 9;
        -ms-grid-column: 1;
    }
    :host([type='list']) .plp-item__info > *:nth-child(10) {
        -ms-grid-row: 9;
        -ms-grid-column: 3;
    }
    :host([type='list']) uc-button-add-to-cart {
      -ms-grid-column: 2;
      -ms-grid-column-span: 1;
      grid-column: 2/3;
      -ms-grid-row: 5;
      -ms-grid-row-span: 1;
      grid-row: 5/6;
    }
    :host([type='list']) ::slotted(uc-plp-item-description) {
      -ms-grid-column: 1;
      -ms-grid-column-span: 1;
      grid-column: 1/2;
      -ms-grid-row: 3;
      -ms-grid-row-span: 1;
      grid-row: 3/4;
    }
    :host([type='list']) uc-plp-item-label {
      -ms-grid-column: 1;
      -ms-grid-column-span: 1;
      grid-column: 1/2;
      -ms-grid-row: 1;
      -ms-grid-row-span: 1;
      grid-row: 1/2;
    }
    :host([type='list']) ::slotted(.plp-item__info__rating) {
      -ms-grid-column: 1;
      -ms-grid-column-span: 1;
      grid-column: 1/2;
      -ms-grid-row: 4;
      -ms-grid-row-span: 1;
      grid-row: 4/5;
      -ms-grid-row-align: start;
          align-self: start;
    }
    :host([type='list']) ::slotted(div) {
      -ms-grid-column: 1;
      -ms-grid-column-span: 1;
      grid-column: 1/2;
      -ms-grid-row: 2;
      -ms-grid-row-span: 1;
      grid-row: 2/3;
      line-height: 1.29;
    }
    :host([type='list']) .prices {
      -ms-grid-column: 2;
      -ms-grid-column-span: 1;
      grid-column: 2/3;
      -ms-grid-row: 1;
      -ms-grid-row-span: 2;
      grid-row: 1/3;
      -ms-grid-row-align: end;
          align-self: end;
    }

    :host([type='list']) .checkboxes {
      -ms-grid-column: 1;
      -ms-grid-column-span: 1;
      grid-column: 1/2;
      -ms-grid-row: 5;
      -ms-grid-row-span: 1;
      grid-row: 5/6;
      -ms-grid-row-align: center;
          align-self: center;
      font-size: 0.85em;
      line-height: 16px;
    }
    :host([type='list']) .checkboxes > .checkboxes__label {
      color: #999999;
      position: relative;
      cursor: pointer;
      padding-left: 24px;
      margin-right: 30px;
      -webkit-transition: color 0.1s ease-in;
      -o-transition: color 0.1s ease-in;
      transition: color 0.1s ease-in;
    }
    :host([type='list']) .checkboxes > .checkboxes__label[checked] {
      color: #444b5b;
    }
    :host([type='list']) .checkboxes > .checkboxes__label:hover {
      color: #333333;
    }
    :host([type='list']) .checkboxes > .checkboxes__label:hover > .label__custom {
      border-color: #333333;
    }
    :host([type='list']) .checkboxes > .checkboxes__label > .label__custom {
      position: absolute;
      top: 0;
      left: 0;
      width: 14px;
      height: 14px;
      border-radius: 2px;
      border: solid 1px #d8d8d8;
      background: transparent;
      -webkit-transition: background-color 0.15s ease-out, border-color 0.15s ease-out;
      -o-transition: background-color 0.15s ease-out, border-color 0.15s ease-out;
      transition: background-color 0.15s ease-out, border-color 0.15s ease-out;
    }
    :host([type='list']) .checkboxes > .checkboxes__label > .label__input {
      position: absolute;
      width: 0;
      height: 0;
      opacity: 0;
      cursor: pointer;
    }
    :host([type='list'])
      .checkboxes
      > .checkboxes__label
      > .label__custom
      > .label__custom__check
      > svg {
      position: relative;
      top: -1px;
      left: -1px;
      width: 16px;
      height: 16px;
      fill: #ffffff;
    }
    :host([type='list']) .checkboxes > .checkboxes__label > .label__input:checked + .label__custom {
      background-color: #414a5e;
      border-color: #414a5e;
    }
    :host([type='list']) uc-button-round {
      margin-right: 20px;
    }

    [hidden] {
      display: none;
    }

    .product-round-btn[added],
    :host(:hover) .product-round-btn {
      opacity: 1;
    }
    .product-round-btn {
      opacity: 0;
    }

    :host([type='list']) ::slotted(uc-image) {
      min-height: 200px;
      min-width: 200px;
    }

    :host(:not([type='list'])) ::slotted(uc-image),
    :host([type='card']) ::slotted(uc-image) {
      width: 100%;
      margin-bottom: 14px;
    }
    :host([type='list']) ::slotted(uc-image) {
      height: 100%;
      margin-right: 16px;
      max-width: 40%;
    }
  }


`;

export const mobile = css`
  @media (max-width: 767px) {
    :host {
      width: 100%;
    }
    ::slotted(uc-plp-item-description),
    .product-round-btn:hover,
    .product-round-btn,
    uc-button-add-to-cart {
      display: none;
    }
    ::slotted(a) {
      padding: 16px;
      -webkit-box-pack: center;
          -ms-flex-pack: center;
              justify-content: center;
    }
    uc-plp-item-label {
      -ms-grid-column: 1;
      -ms-grid-column-span: 1;
      grid-column: 1/2;
      -ms-grid-row: 1;
      -ms-grid-row-span: 1;
      grid-row: 1/2;
    }
    ::slotted(uc-image) {
      max-width: 40%;
      margin-right: 16px;
      margin-bottom: 0px;
    }
    .checkboxes {
      display: none;
    }
  }


`;
