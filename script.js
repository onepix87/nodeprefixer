const fs = require('fs');
const autoprefixer = require('autoprefixer');
const postcss = require('postcss');
const cssvariables = require('postcss-css-variables');
const glob = require('glob');
const babel = require('@babel/core');

console.log(babel.transformFile('comp2.js', {}, (err)=>{
    if(err) throw err;
}));
// glob("styles/*.style.css", {},(error, filesArr)=>{
//     if(error) throw error;
//     filesArr.forEach(filePath=>{
//         fs.readFile(filePath, "utf8", (err, fileString) => {
//             if(err) throw err;
//             postcss([cssvariables({preserve: 'computed'}), autoprefixer({grid: 'autoplace'})]).process(fileString).then(result => {
//                 const paramsWithStyleArr = result.css.match(/\/\*[^\*]*\*\/([^\/])*/g);
//                 const paramsArr = paramsWithStyleArr.map(paramAndStyle=>{
//                     let comment = paramAndStyle.match(/\/\*[^\*]*\*\//g)[0];
//                     let param = comment.substring(2, comment.length-2);
//                     let style = 'css`' + paramAndStyle.replace(comment, '') + '`';
//                     return [param, style];
//                 });
//                 const componentFilePath =  filePath.replace('style.css', 'js');
//                 fs.readFile(componentFilePath, "utf8", (err, fileString) => {
//                     if(err) throw err;
//                     paramsArr.forEach(paramAndStyle => {
//                         fileString = fileString.replace(paramAndStyle[0], paramAndStyle[1]);
//                     });
//                     fs.writeFile(componentFilePath, fileString, err =>{
//                         if(err) throw err;
//                     })
//                 })
//             }).catch(err => {
//                 throw err;
//             });
//         })
//     });
// });