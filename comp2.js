import { css } from 'astroturf';

function mobile(content) {
    return css`
    @media screen and (max-width: 767px) {
      ${content}
    }
  `;
}

export const styles = css`
  :host {
    display: grid;
    grid-template-areas: 'title' 'description' 'image' 'offerlink';
    grid-template-rows: max-content max-content 1fr;
    grid-template-columns: 1fr;
  }

  .title-wrap {
    display: flex;
    grid-area: title;
    align-items: flex-end;
  }

  .title::slotted(*) {
    margin: 0 0 36px;
    color: #484e5c;
    font-weight: 500;
    font-size: 22px;
    line-height: 25px;
  }

  .description-wrap {
    grid-area: description;
  }

  .description::slotted(*) {
    margin: 0;
    color: #484e5c;
    font-size: 14px;
    line-height: 1.71;
  }

  .image-wrap {
    grid-area: image;
  }

  .image::slotted(*) {
    width: 100%;
    height: auto;
  }

  .link-wrap {
    display: flex;
    grid-area: offerlink;
    justify-content: center;   
    margin-top: 30px;
  }

  .offer-slot::slotted(*) {
    padding: 16px 30px;
  }

  ${mobile(css`
    .description {
      display: none;
    }

    .offer-link,
    .offer-slot::slotted(*) {
      width: 100%;
    }
  `)}
`;
